## Set up

1. clone git repository
2. composer install
3. copy .env.example to .env file
4. php artisan key:generate
5. npm install
6. npm run dev
7. php artisan serve


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
software licensed under the [MIT license](https://opensource.org/licenses/MIT).
