<?php

namespace App\Http\Controllers;

use App\Utils\NbpClass;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Mockery\Exception;

class CurrencyController extends Controller
{
    private $data;
    private $amount;
    private $converted;

    public function convertCurrency(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric',
        ]);

        $this->amount = $request->get('amount');

        try {
            $this->callApi();
        } catch (Exception $e) {
            return response()
                ->json(['front_message' => ['error' => 'Error : Cannot get currency rate']], 520);
        }

        $this->convertAmount();

        return response()
            ->json(['front_message' => ['info' => 'Success', 'eur' => $this->converted]], 200);
    }

    private function callApi()
    {
        $client = new Client();
        $res = $client->request('GET', NbpClass::NBP_EURO_RATE_URI);
        if ($res->getStatusCode() != '200') {
            return response()
                ->json(['front_message' => ['error' => 'Error : Cannot get currency rate']], 520);
        }

        $responseData = $res->getBody()->getContents();
        $this->data = json_decode($responseData, TRUE);
    }

    private function convertAmount()
    {
        if ($this->data['code'] == 'EUR') {
            $rate = $this->data['rates'][0]["mid"];
        } else {
            return response()
                ->json(['front_message' => ['error' => 'Error : Cannot get currency rate']], 520);
        }

        $this->converted = number_format((float)($this->amount/$rate), 2, '.', '');
    }

}
